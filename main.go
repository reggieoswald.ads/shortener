package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"shortener/pkg/domain"
	"shortener/pkg/services"
	"strings"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	shortener := services.NewShortener()

	http.HandleFunc("/url", generateURL(shortener))
	http.HandleFunc("/", redirect(shortener))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

func generateURL(shortener domain.Shortener) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		val, ok := r.URL.Query()["url"]
		if !ok {
			http.Error(w, "provide valid url", http.StatusBadRequest)
			return
		}

		if len(val) != 1 {
			http.Error(w, "provide valid url", http.StatusBadRequest)
			return
		}

		shortedURL, err := shortener.ShortURL(val[0])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprint(w, shortedURL)
	}
}

func redirect(shortener domain.Shortener) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		val := r.URL.Path
		if val == "" {
			http.Error(w, "provide valid url", http.StatusBadRequest)
			return
		}

		validURL, err := shortener.GetURL(strings.Trim(val, "/"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, validURL, http.StatusMovedPermanently)
	}
}
