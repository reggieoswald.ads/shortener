package services

import (
	"fmt"
	"math/rand"
	"shortener/pkg/domain"
)

type shortener struct {
	cache  map[string]string // key - short / val - url
	cache2 map[string]string //
}

func NewShortener() domain.Shortener {
	return &shortener{
		cache:  make(map[string]string),
		cache2: make(map[string]string),
	}
}

func (s *shortener) ShortURL(url string) (shortedURL string, err error) {
	if val, ok := s.cache2[url]; ok {
		val = fmt.Sprintf("http://localhost:8080/%s", val)
		return val, nil
	}

	shortedURL = s.getUnique(1)

	s.cache[shortedURL] = url
	s.cache2[url] = shortedURL

	shortedURL = fmt.Sprintf("http://localhost:8080/%s", shortedURL)

	return shortedURL, err
}

func (s *shortener) GetURL(url string) (string, error) {
	if val, ok := s.cache[url]; ok {
		return val, nil
	} else {
		return "", fmt.Errorf("no value for url: %s", url)
	}
}

func (s *shortener) getUnique(ln int) string {
	b := make([]rune, ln)

	for i := 0; i < 10; i++ {
		for j := range b {
			b[j] = runes[rand.Intn(len(runes))]
		}

		if _, ok := s.cache[string(b)]; !ok {
			return string(b)
		}
	}

	ln++
	return s.getUnique(ln)
}

var runes = []rune("abcdefABCDEF")
