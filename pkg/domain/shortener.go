package domain

type Shortener interface {
	ShortURL(url string) (string, error)
	GetURL(url string) (string, error)
}
